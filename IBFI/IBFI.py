'''Core code for python implementation of Image Based Force Inference (IBFI).
Computes Standard Tensions and Standard Pressures from a CellNetwork object.

Copyright February 2014.
All rights reserved.

Authors:
David Mashburn
Jim Veldhuis
Wayne Brodland
Shane Hutson'''

from __future__ import print_function,division

import os
from copy import deepcopy
import numpy as np
import scipy.optimize
import scipy.sparse

from np_utils import ( linearTransform, reverseLinearTransform,
                       diffmean, sqrtSumSqr, pointDistance,
                       vectorNorm, polyCentroid,
                       flatten, ziptranspose, )

from ImageContour.SubContourTools import CellNetwork

def GetGreaterPressureCell1ForAllSCIDs( allSCIDs,scCellIDs,cellCentersByCellID,
                                        chordMidpoints,circleCenters,bgVals ):
    '''For each subContour, compute whether it is bulging out from the
       first or second cell by comparing the chordal midpoint with the
       best-fit circle center and the cell center.
       
       Returns a list with elements:
         True if the first cell has greater pressure
         False if the second cell has greater pressure
         None in the case where this edge borders the background'''
    grPressCell1 = []
    for scid in allSCIDs:
        cellIDs = scCellIDs[scid]
        if set(cellIDs).intersection(bgVals):
            grPressCell1.append(None)
        else:
            cellCenters = [ cellCentersByCellID[cid] for cid in cellIDs ]
            chordMidpoint = chordMidpoints[scid]
            circleCenter = circleCenters[scid]
            # Compute greaterPressureCell1
            gp1 = np.dot(chordMidpoint-circleCenter,chordMidpoint-cellCenters[0]) > 0
            grPressCell1.append(gp1)
    return grPressCell1

def colinearTest(points):
    '''Returns True if points are colinear'''
    if len(points)<2:
        return True
    vectors = points[1:]-points[0]
    vectorDirs = vectorNorm(vectors)
    if np.all(np.abs(vectorDirs[1:]-vectorDirs[0])<1e-6):
        return True
    return False

def FitCircleToPoints(points,fitEndpoints=False,fitFirstPoint=False):
    '''Given a set of points, return the center and radius
       of the best fit circle.
       
       If fitEndpoints is True, the circle will pass through both endpoints.
       If fitFirstPoint is True, the circle will pass through the first point.
       
       Based heavily on from scipy wiki's "Least_Squares_Circle"
       http://wiki.scipy.org/Cookbook/Least_Squares_Circle'''
    points = np.asarray(points)
    p0,pf = points[0],points[-1] # get the two enpoints
    pm = 0.5*(p0+pf)             # get the midpoint between the endpoints
    
    if colinearTest(points):
        return pm,None
    
    if fitEndpoints:
        innerPts = points[1:-1]                                         # get all the interior points (no end points)
        innerPointsTrans = reverseLinearTransform(innerPts,[pm,pf])     # Change the reference frame of the points from pm->pf to (0,0)->(1,0)
                                                                        #   so that the circle center will always lie on the y-axis
                                                                        #   this also means the new p0 will be at (-1,0)
        testFun = lambda yc: ( pointDistance(innerPointsTrans,[0,yc]) - # given a circle center, compute (distance to each point) - (circle radius)
                               pointDistance([-1,0],[0,yc]) )           #   where the circle radius is the distance to the first point
        y_estimate = -innerPointsTrans[:,1].mean()                      # Estimate y as the negative of the mean y-value of the points
        y,ier = scipy.optimize.leastsq(testFun,y_estimate)              # Optimize the test function to find the best estimate for y
        center = linearTransform([0,y],[pm,pf])                         # Transorm the circle center in the new reference frame (point [0,y])
                                                                        #   back to the original reference frame
    else:
        testFunFirst = lambda c: ( pointDistance(points,c) -            # given a circle center, compute (distance to each point) - (circle radius)
                                   pointDistance(points[0],c) )         #   where the circle radius is the distance to the first point
        testFunMean = lambda c: diffmean(pointDistance(points,c))       # given a circle center, compute (distance to each point) - (circle radius)
                                                                        #   where the circle radius is just the average distance for all the points
        
        testFun = ( testFunFirst if fitFirstPoint else
                    testFunMean )
        
        center_estimate = points.mean(axis=0)                           # Use the mean point as the center estimate
        center,ier = scipy.optimize.leastsq(testFun,center_estimate)    # Optimize the test function to find the best estimate of the circle center
    
    radius = ( pointDistance(center,p0)
               if fitEndpoints or fitFirstPoint else
               pointDistance(center,points).mean() )
    
    return center,radius

def getCircleTangentDirection(points,centerAndRadius=None):
    '''Take a list of pointIDs where the first is a triple junction and
       compute the normalized direction tangent to the best fit circle.'''
    center,radius = ( centerAndRadius if centerAndRadius!=None else     # Don't recompute center and radius if it is passed
                      FitCircleToPoints(points,fitEndpoints=True) )
    tj,nnv = points[0],points[1]-points[0]                              # Get the coordinates of the TJ and the vector to the nearest neighbor
    if radius==None:                                                    # If the circle fit failed,
        return vectorNorm(nnv)                                          #   Return the nearest neighbor vector
    else:                                                               # Otherwise,
        tangent = vectorNorm(np.dot( [tj-center], [[0,1],[-1,0]] )[0])  #   Get a vector tangent to the circle using a 90 degree rotation
        s = ( 1 if np.dot(tangent,nnv) >= 0 else -1 )                   #   Determine the sign of tangent vector . nearest neighbor vector
        return s*tangent                                                #   Return the tangent vector aligned with the nearest neighbor vector

def getPolyTangentDirection(points,order=2,fitFirst=True,fitLast=False):
    points = np.asarray(points)
    nPts = len(points)
    # Set up the weights
    w = np.ones(nPts)
    if fitFirst: w[0]=1e10
    if fitLast:  w[-1]=1e10
    # Get the polynomial fits for X&Y
    fitsXY = [ np.polyfit(range(nPts),points[:,i],order,w=w)
              for i in range(2) ]
    dFits = [ np.polyder(f) for f in fitsXY ] # Compute the polynomial derivative
    tangentVec = [ df[-1] for df in dFits ]       # And select the linear term coefficient, which is the tangent direction
    return vectorNorm(tangentVec)                 # Return a normalized vector

def IsLocallyConsistent(tjDVecs):
    '''Perform a local solve for local consistency'''
    a = np.concatenate([ np.transpose(tjDVecs), [[1,0,0]] ])
    try:
        if np.any( np.linalg.solve(a,[0,0,1]) < 0 ):
            return False
    except np.linalg.LinAlgError:
        return False
    return True

def GetVectorDirectionsAtTJs(pointsArr,tjSCIDs,tjSCPids_flipped,centers,radii,nFitPointsForAngles=20):
    '''For each triple junction, compute the vector direction of each
       subContours connected to it, using three different measures.
       
       Takes the following variables:
       ------------------------------
       pointsArr:        Array of all points
       tjSCIDs:          List of subContour ID's for each triple junction
                         shape (nTJs,<# connected sc's>)
       tjSCPids_flipped: List of lists of point ID's starting on each TJ
                         shape (nTJs,<# connected sc's>,<# pts>)
       centers:          Best-fit circle centers for each subContour
       radii:            Best-fit circle radii for each subContour
       
       Returns:
       --------
       ( nnDirections,chordDirections,
         circleTangentDirections,limCircleTangentDirections,
         paraTangentDirections,limParaTangentDirections )
       Each has shape (nTJs,<# connected sc's>,2)'''
    empty = [ [ [] for scid in scids ]
             for scids in tjSCIDs ]
    nnDirections               = deepcopy(empty)
    chordDirections            = deepcopy(empty)
    circleTangentDirections    = deepcopy(empty)
    limCircleTangentDirections = deepcopy(empty)
    paraTangentDirections      = deepcopy(empty)
    limParaTangentDirections   = deepcopy(empty)
    
    for i,scids,pidsList in zip( range(len(tjSCIDs)), tjSCIDs, tjSCPids_flipped ):
        for j,scid,pids in zip( range(len(scids)), scids,pidsList ):
            center,radius = (centers[scid],radii[scid])
            pts = [pointsArr[pid] for pid in pids]
            
            nnDirections[i][j]               = vectorNorm(pointsArr[pids[1]]-pointsArr[pids[0]])
            chordDirections[i][j]            = vectorNorm(pointsArr[pids[-1]]-pointsArr[pids[0]])
            circleTangentDirections[i][j]    = getCircleTangentDirection(pts,(center,radius))
            limCircleTangentDirections[i][j] = getCircleTangentDirection(pts[:nFitPointsForAngles])
            paraTangentDirections[i][j]      = getPolyTangentDirection(pts)
            limParaTangentDirections[i][j]   = getPolyTangentDirection(pts[:nFitPointsForAngles])
    
    return ( nnDirections,chordDirections,
             circleTangentDirections,limCircleTangentDirections,
             paraTangentDirections,limParaTangentDirections )

def _interiorTest(scid,scCellIDs,bgVals,excludeBoundary=True):
    '''Returns False if a cell is on the boundary, True otherwise
       But, always returns True if excludeBoundary is False'''
    cidsSet = set(scCellIDs[scid])
    if not excludeBoundary:                                             # Always True if excludeBoundary is False
        return True
    elif len(cidsSet)==2 and not cidsSet.intersection(bgVals):          # SC is interior if there are 2 cellIDs, neither in the backgound
        return True
    else:                                                               # Otherwise, it's on the background
        return False

def GetUsedPointsSubContoursAndCells( allSCIDs,allCellIDs,bgVals,
                                      tjPids,tjSCIDs,tjDVecs,
                                      scEndPids,scCellIDs,scLengths,
                                      lengthCutoff=0,
                                      excludeBoundary=True,
                                      excludeCellsMissingTension=True,
                                      enforceLocalConsistency=True ):
    '''Build mappings from original ID's to matrix indices for
       points, subContours, and cells
       
       Takes the following variables:
       ------------------------------
       tjSCIDs:   List of subContour ID's for each triple junction
                  shape (nTJs,<# connected sc's>)
       tjDVecs:   Direction vectors at each triple junction
       scEndPids: Point ID's of the endpoints in each subContour
       scCellIDs: For each subContour, a list of connected cell ID's
       
       Returns usedPointIDs,usedSCIDs,usedCellIDs'''
    # Get a list of all the locally self-consistent sc ID's
    newTjSCIDs = ( tjSCIDs if not enforceLocalConsistency else      # Allow junctions that are not locally consistent
                   [ scids for scids,tdirs in zip(tjSCIDs,tjDVecs)  # For each TJ, get the connecting sc id's and directions
                           if IsLocallyConsistent(tdirs) ] )        # ignore junctions that are not locally consistent
    
    # Get a set of all sc id's to use
    usedSCID_set = { scid
                    for scids in newTjSCIDs
                    for scid in scids                                         # for each sc connected to the tj
                    if scLengths[scid]>lengthCutoff                           # ignore anything shorter than the length cutoff
                    if _interiorTest(scid,scCellIDs,bgVals,excludeBoundary) } # ignore all edges on the boundary
    usedSCIDs = sorted(usedSCID_set)
    unusedSCIDs = sorted(set(allSCIDs)-usedSCID_set)
    # Get a list of the point ID's of the triple junctions to use in the calculation
    unusedPointID_set = set(flatten([ scEndPids[scid] for scid in unusedSCIDs ]))
    unusedPointIDs = sorted(unusedPointID_set)
    usedPointIDs = sorted(set(tjPids)-unusedPointID_set)
    
    # Get a list of all the cell ID's to use in the calculation
    usedCellIDs = ( sorted( set(allCellIDs) -
                            set(flatten([ scCellIDs[scid]
                                         for scid in unusedSCIDs ])) )
                    if excludeCellsMissingTension else
                    sorted(set(flatten([ scCellIDs[scid]
                                        for scid in usedSCIDs ]))) )
    
    
    return usedPointIDs,usedSCIDs,usedCellIDs

def _computeResultMeasures(X,rhs,XTX,XTXi,solution,clip=-1):
    nEqns,nUnknowns = X.shape
    conditionNumber = np.sqrt(np.linalg.cond(XTX))
    residuals = rhs - np.dot(X,solution[:clip]) # residuals = X . solutions
    sumOfSquaredResiduals = np.sum(residuals**2)
    varianceCovarianceMatrix = ( sumOfSquaredResiduals/(nEqns-nUnknowns) ) * XTXi[:clip,:clip]
    standardErrors = np.sqrt(np.diag(varianceCovarianceMatrix))
    
    return { 'nEqns':nEqns,
             'nUnknonwns':nUnknowns,
             'Matrix':scipy.sparse.coo_matrix(X),
             'conditionNumber':conditionNumber,
             'solution':solution,
             'residuals':residuals,
             'standardErrors':standardErrors,
             'sumOfSquaredResiduals':sumOfSquaredResiduals,
           }

def _remapRawValues(rawValues,allIDs,usedIDs):
    valuesByID = { i:t for i,t in zip(usedIDs,rawValues) }
    values = [ ( valuesByID[i] if i in valuesByID else None )
              for i in allIDs ]
    return values

def GetGMatrix(nRows,nCols,usedTJIDs,usedSCIDs,tjSCIDs,tjDVecs,matrixSaveDir=None):
    '''Compute the Geometry Matrix'''
    SCIDtoMatrixInd = { scid:i for i,scid in enumerate(usedSCIDs) }
    
    G = np.zeros([nRows, nCols],np.double)
    for i,tji in enumerate(usedTJIDs):
        scids = tjSCIDs[tji]
        for j,scid in enumerate(scids):
            if scid in usedSCIDs:
                col = SCIDtoMatrixInd[scid]
                G[2*i:2*i+2, col] = tjDVecs[tji][j]
    if matrixSaveDir:
        np.savetxt(os.path.join(matrixSaveDir,'G.txt'),G)
    return G

def GetPMatrix(nRows,nCols,usedSCIDs,usedCellIDs,scCellIDs,grPressCell1,matrixSaveDir=None):
    CellIDtoMatrixInd = { cid:i for i,cid in enumerate(usedCellIDs) }
    
    # Build the P matrix
    P = np.zeros([nRows, nCols],np.double)
    for i,scid in enumerate(usedSCIDs):
        cids = scCellIDs[scid]
        grPC1 = grPressCell1[scid]
        signMul = ( None if grPC1==None else
                    1    if grPC1 else
                    -1 )
        if signMul!=None:
            for cid,m in zip(cids,(1,-1)):
                if cid in usedCellIDs:
                    col = CellIDtoMatrixInd[cid]
                    P[i,col] = m*signMul
    if matrixSaveDir:
        np.savetxt(os.path.join(matrixSaveDir,'P.txt'),P)
    return P

def GetIBFITensions(allSCIDs,usedTJIDs,usedSCIDs,tjSCIDs,tjDVecs,matrixSaveDir=None):
    '''Calculate the tensions for IBFI
       
       Takes the following variables:
       ------------------------------
       allSCIDs:     List of all the subContour ID's from the cellNetwork
       usedTJIDs:    Indices of all TJ's used in the calculation
       usedSCIDs:    ID's for all subContours to use in the calculation
       tjSCIDs:      List of subContour ID's for each triple junction
                     shape (nTJs,<# connected sc's>)
       tjDVecs:      Direction vectors at each triple junction
       
       Returns:
       --------
       tensions:       The tension in each subContour
       tensionErrors:  The standard error of each subContour tension
       tensionResults: A dictionary with a number of variables:
           ...<more here>...'''
    nTjs,nSCs = len(usedTJIDs),len(usedSCIDs)
    nRows,nCols = nTjs*2,nSCs
    nEqns,nUnknowns = nRows,nCols
    
    # Build the G matrix
    G = GetGMatrix(nRows,nCols,usedTJIDs,usedSCIDs,tjSCIDs,tjDVecs,matrixSaveDir)
    print( "Solving for %d edge Tensions with %d X 2 = %d Equations\n" % (nSCs, nTjs, nEqns) )
    GTG = np.zeros([nCols+1,nCols+1],dtype=G.dtype)
    GTrhs = np.zeros([nCols+1,1],dtype=G.dtype)
    GTG[:nCols,:nCols] = np.dot(G.T,G)
    
    # Set the Lagrange constraints
    GTG[nCols,:] = 1
    GTG[:,nCols] = 1
    GTrhs[nCols] = nCols
    
    # Solve for the tensions
    GTGi = np.linalg.inv(GTG)
    tensionsRaw = np.dot(GTGi,GTrhs) # np.linalg.solve(GTG, GTrhs)
    tensionResults = _computeResultMeasures(G,0,GTG,GTGi,tensionsRaw)
    tensions = _remapRawValues( tensionsRaw[:-1,0].tolist(), allSCIDs, usedSCIDs )
    tensionErrors = _remapRawValues( tensionResults['standardErrors'], allSCIDs, usedSCIDs )
    return tensions,tensionErrors,tensionResults

def GetIBFIPressures( allCellIDs,usedSCIDs,usedCellIDs,scCellIDs,
                      grPressCell1,curvatures,tensions,
                      matrixSaveDir=None ):
    '''Calculate the pressures for IBFI
       
       Takes the following variables:
       ------------------------------
       allCellIDs:   List of all the cell ID's from the cellNetwork
       usedSCIDs:    ID's for all subContours to use in the calculation
       usedCellIDs:  ID's for all cells to use in the calculation
       scCellIDs:    For each subContour, a list of connected cell ID's
       grPressCell1: For each subContour, whether cell #1 is bulging out, in, or is flat (False/True/None)
       curvatures:   Curvatures of the best-fit circles
       tensions:     Tensions for each subContour (calculated in GetIBFITensions)
       
       Returns:
       --------
       pressures:       The pressure in each cell
       pressureErrors:  The standard error of each cell pressure
       pressureResults: A dictionary with a number of variables:
           ...<more here>...'''
    nSCs,nCells = len(usedSCIDs),len(usedCellIDs)
    nRows,nCols = nSCs,nCells
    nEqns,nUnknowns = nRows,nCols
    
    # Build the P matrix
    P = GetPMatrix(nRows,nCols,usedSCIDs,usedCellIDs,scCellIDs,grPressCell1,matrixSaveDir)
    print( "Solving for %d cell Pressures with %d Equations\n" % (nCells, nEqns) )
    PTP = np.zeros([nCols+1,nCols+1],dtype=P.dtype)
    PTrhs = np.zeros([nCols+1,1],dtype=P.dtype)
    PTP[:nCols,:nCols] = np.dot(P.T,P)
    rhs = np.array([ ( 0 if curvatures[scid]==None else
                       tensions[scid]*curvatures[scid] )
                    for scid in usedSCIDs ])
    PTrhs[:nCols,0] = np.dot(P.T,rhs)
    
    # Set the Lagrange constraints
    PTP[nCols,:] = 1
    PTP[:,nCols] = 1
    PTrhs[nCols] = 0
    
    # Solve for the pressures
    PTPi = np.linalg.inv(PTP)
    pressuresRaw = np.dot(PTPi,PTrhs) # np.linalg.solve(PTP, PTrhs)
    pressureResults = _computeResultMeasures(P,rhs,PTP,PTPi,pressuresRaw)
    pressures = _remapRawValues( pressuresRaw[:-1,0].tolist(), allCellIDs, usedCellIDs )
    pressureErrors = _remapRawValues( pressureResults['standardErrors'], allCellIDs, usedCellIDs )
    return pressures,pressureErrors,pressureResults

def IBFI( cellNetwork,
          bgVals=(0,1),
          nFitPointsForAngles=20,
          lengthCutoff=0,
          excludeBoundary=True,
          excludeCellsMissingTension=True,
          enforceLocalConsistency=True,
          matrixSaveDir=None ):
    '''Image-Based Force Inference
       Takes polygonal network (in the form of a CellNetwork)
       and computes the Standard Tensions and Standard Pressures using
       the algorithm described in <Brodland et al 2014>.'''
    cn = cellNetwork  # alias cellNetwork as cn
    
    # Set up the geometric variables:
    # -------------------------------
    # Variables for points:
    points = cn.GetAllPoints()                                          # Get a consistent set of mesh points that can be indexed against
    pointsArr = np.array(points)                                        # Array version of points
    allPointIDs = range(len(points))                                    # Get the ID for every point
    pidByPoint = { p:pid for pid,p in zip(allPointIDs,points) }         # Given any x-y point, get its index (point ID/pid)
    
    # Variables for subContours (cell-cell interfaces):
    allSCIDs = range(len(cn.subContours))                               # Get the ID for every subContour (or cell-cell interface)
    scLengths = cn.GetSubContourLengths()                               # For each subContour, sum the lengths of all the segments
    scPids = [ [ pidByPoint[tuple(p)]                                   # For each subContour (cell-cell interface), get the index of its points in the "points" list
                for p in sc.points ]
              for sc in cn.subContours ]
    scEndPids = [ [pids[0],pids[-1]] for pids in scPids ]               # Get the index of the first and last point in each subContour
    chordMidpoints = np.mean(pointsArr[scEndPids,],axis=-1)             # Compute the midpoints of each chord (mean of the midpoints)
    centers,radii = np.transpose([ FitCircleToPoints(sc.points,fitEndpoints=True)  # Fit a circle to each subContour and save the centers and radii
                                  for sc in cn.subContours ])
    curvatures = [ ( None if r==None else 1./r )                        # Compute the curvatures (1/radii)
                  for r in radii ]
    scCellIDs = [ sc.values for sc in cn.subContours ]                  # For each subContour, get the cellIDs
    
    # Variables for cells:
    allCellIDs = cn.GetAllValues(bgVals=bgVals)                         # Get the ID for every cell
    cellCentersByCellID = { cid:ctr                                     # Get the list of cell centroids
                           for cid,ctr in zip(allCellIDs,cn.GetCellCentroids()) }
    
    # Variables for triple junctions (points where three cells meet):
    tripleJunctions = cn.GetJunctionsByNSubContours()[3]                # Get a list of all the Junction objects with 3 connecting subContours
    tjPids = [ pidByPoint[j.point] for j in tripleJunctions ]           # Get the point indices that are triple junctions (TJ's) (all the endpoints in the subContours)
    tjSCIDs = [ j.subContourIDs for j in tripleJunctions ]              # Get all the connecting subContours IDs for each triple junction
                                                                        #   shape is:  (nTJs,<varying number of connected sc's>)
    tjIndexByPid = { pid:i for i,pid in enumerate(tjPids) }             # Get a mapping from TJ pid to TJ index
    tjSCPids_flipped = [ [ ( pids if tjpids==pids[0] else pids[::-1] )  # For each TJ, get the subContour's pids always radiating away from the TJ (TJ pid will always come first)
                          for scid in tjSCIDs[tji]                      #   shape is:  (nTJs,<varying number of connected sc's>,<varying number of pids>)
                          for pids in [ scPids[scid] ] ]
                        for tji,tjpids in enumerate(tjPids) ]
    
    # For each subContour, determine if its first neighboring cell is
    # the one with the greater pressure (bulging out)
    grPressCell1 = GetGreaterPressureCell1ForAllSCIDs( allSCIDs,scCellIDs,cellCentersByCellID,
                                                       chordMidpoints,centers,bgVals )
    
    # For each TJ, get the direction vectors for each connected subContour
    nnD,chordD,cTanD,limCTanD,pTanD,limPTanD = GetVectorDirectionsAtTJs( pointsArr,tjSCIDs,tjSCPids_flipped,centers,radii,
                                                                         nFitPointsForAngles=nFitPointsForAngles )
    tjDVecs = limPTanD # Choose one measure as the direction vectors list, "tjDVecs"
    
    # Build mappings from original ID's to matrix indices
    usedPointIDs,usedSCIDs,usedCellIDs = GetUsedPointsSubContoursAndCells( allSCIDs,allCellIDs,bgVals,
                                                                           tjPids,tjSCIDs,tjDVecs,
                                                                           scEndPids,scCellIDs,scLengths,
                                                                           lengthCutoff=lengthCutoff,
                                                                           excludeBoundary=excludeBoundary,
                                                                           excludeCellsMissingTension=excludeCellsMissingTension,
                                                                           enforceLocalConsistency=enforceLocalConsistency )
    usedTJIDs = [ tjIndexByPid[pid] for pid in usedPointIDs ]
    # Calculate the tensions
    tensions,tensionErrors,tensionResults = GetIBFITensions( allSCIDs,usedTJIDs,usedSCIDs,tjSCIDs,tjDVecs,
                                                             matrixSaveDir=matrixSaveDir )
    
    # Calculate the pressures
    pressures,pressureErrors,pressureResults = GetIBFIPressures( allCellIDs,usedSCIDs,usedCellIDs,scCellIDs,
                                                                 grPressCell1,curvatures,tensions,
                                                                 matrixSaveDir=matrixSaveDir )

    return tensions,pressures,tensionErrors,pressureErrors,tensionResults,pressureResults
