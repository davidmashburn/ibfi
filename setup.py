from distutils.core import setup

# Read the version number
with open("IBFI/_version.py") as f:
    exec(f.read())

setup(
    name='IBFI',
    version=__version__, # use the same version that's in _version.py
    author='David N. Mashburn',
    author_email='david.n.mashburn@gmail.com',
    packages=['IBFI'],
    scripts=[],
    url=''#'http://pypi.python.org/pypi/IBFI/',
    license='LICENSE.txt',
    description='Algorithm for Image Based Force Inference',
    long_description=open('README.rst').read(),
    install_requires=[
                      'numpy>=1.0',
                      'scipy>=1.0',
                      'ImageContour>=0.2.3.0',
                      'np_utils>=0.3.2.0'
                     ],
)
